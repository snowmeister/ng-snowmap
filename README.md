# NgSnowmap

This project continues my mission to get up to speed with the lastest Big Three JavaScript frameworks (React, VueJs and Angular), this time focussing on getting up to speed with Angular.

To see my VueJS exploratory app, see the SnowMap app - [repo](https://bitbucket.org/snowmeister/snowmap) | [demo](https://map.snowmeister.co.uk)

To see my ReactJS exploratory app, see the AroundMe app - [repo](https://bitbucket.org/snowmeister/snowmapmobile) | [demo](https://aroundme.snowmeister.co.uk)

This app is my first foray into Angular since building my personal portfolio site with Angular 1 back in 2012/13 - I have to admit I found it hard going back then, but more hopeful now we are looking at v4.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.2.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
