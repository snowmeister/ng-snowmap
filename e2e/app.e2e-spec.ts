import { NgSnowmapPage } from './app.po';

describe('ng-snowmap App', () => {
  let page: NgSnowmapPage;

  beforeEach(() => {
    page = new NgSnowmapPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
