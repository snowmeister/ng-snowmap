import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class PostcodeService {

  constructor(private http: Http) {}

  getLatLngFromPostcode(postcode: string) {
    const postcodeApiUri = 'https://api.postcodes.io/postcodes/' + postcode;
    return this.http.get(postcodeApiUri)
    .map( res => res.json() );
  }

  getPostcodeFromLatLng(lat: number, lng: number) {
    const postcodeApiUri = 'https://api.postcodes.io/postcodes?lon=' + lng + '&lat=' + lat;
    return this.http.get(postcodeApiUri)
    .map( res => res.json() );
  }

}
