import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
@Injectable()
export class CrimeService {

  constructor(private http: Http) {}

  getCrime(lat: number, lng: number) {
    const url = 'https://data.police.uk/api/crimes-street/all-crime?lat=' + lat + '&lng=' + lng;
    return this.http.get(url)
    .map( res => res.json() );
  }

}
