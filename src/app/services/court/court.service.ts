import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class CourtService {
  constructor(private http: Http) { }
  getCourts(postcode: string) {
    const url = 'https://aroundme.snowmeister.co.uk/proxy.php?url=https://map.snowmeister.co.uk/courts/' + postcode;
    return this.http.get(url)
      .map(res => res.json());
  }
}
