import { Injectable } from '@angular/core';
import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';

@Injectable() export class LocationService {

    constructor() { }

    /**
     * Tries HTML5 geolocation.
     *
     *  Geolocation API wrapped into an observable.
     *
     * @return An observable of Position
     */
    getLocation(): Observable<Position> {
        return new Observable((observer: Observer<Position>) => {
            // Invokes getCurrentPosition method of Geolocation API.
            navigator.geolocation.getCurrentPosition(
                (position: Position) => {
                    observer.next(position);
                    observer.complete();
                },
                (error: PositionError) => {
                    observer.error(error);
                }
            );
        });
    }

}
