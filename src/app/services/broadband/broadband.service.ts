import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
@Injectable()
export class BroadbandService {
  constructor(private http: Http) { }
  getBroadband(postcode: string) {
    const url = 'https://map.snowmeister.co.uk/broadband/' + postcode.replace(' ', '');
    return this.http.get(url)
      .map(res => res.json());
  }
}
