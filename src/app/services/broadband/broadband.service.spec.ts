import { TestBed, inject } from '@angular/core/testing';

import { BroadbandService } from './broadband.service';

describe('BroadbandService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BroadbandService]
    });
  });

  it('should be created', inject([BroadbandService], (service: BroadbandService) => {
    expect(service).toBeTruthy();
  }));
});
