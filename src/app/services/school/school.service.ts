import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class SchoolService {

  constructor(private http: Http) { }

  getSchools(postcode: string) {
    const url = 'https://map.snowmeister.co.uk/schools/' + postcode;
    return this.http.get(url)
      .map(res => res.json());
  }
}
