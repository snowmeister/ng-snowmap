// MODULES HERE...
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
// Then COMPONENTS...
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { ContactComponent } from './components/contact/contact.component';
import { PostcodeformComponent } from './components/postcodeform/postcodeform.component';
import { MainComponent } from './components/main/main.component';
import { CrimeItemComponent } from './components/crime/crime-item/crime-item.component';
import { CrimeTitleComponent } from './components/crime/crime-title/crime-title.component';
import { DistanceAwayComponent } from './components/common/distance-away/distance-away.component';
import { SchoolItemComponentComponent } from './components/school/school-item-component/school-item-component.component';
import { CourtItemComponent } from './components/court/court-item/court-item.component';

import { PostcodeService } from './services/postcode/postcode.service';
import { SchoolService } from './services/school/school.service';
import { CrimeService } from './services/crime/crime.service';
import { CourtService } from './services/court/court.service';
import { BroadbandService } from './services/broadband/broadband.service';
import { LocationService } from './services/location/location.service';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'main', component: MainComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    ContactComponent,
    PostcodeformComponent,
    MainComponent,
    CrimeItemComponent,
    CrimeTitleComponent,
    DistanceAwayComponent,
    SchoolItemComponentComponent,
    CourtItemComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [PostcodeService, LocationService, CrimeService, BroadbandService, SchoolService, CourtService],
  bootstrap: [AppComponent]
})
export class AppModule { }
