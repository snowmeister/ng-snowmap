import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Ng-Snowmap';
  description = 'Howdy! Welcome to Snowmeister\'s Angular exploration app.';
}
