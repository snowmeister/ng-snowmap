import { Component, OnInit } from '@angular/core';
import { CrimeService } from '../../services/crime/crime.service';
import { BroadbandService } from '../../services/broadband/broadband.service';
import { SchoolService } from '../../services/school/school.service';
import { CourtService } from '../../services/court/court.service';
import { CrimeItemComponent } from '../crime/crime-item/crime-item.component';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})

export class MainComponent implements OnInit {

  ready: Boolean = false;
  currentSet: String = 'main';
  crimes: Array<{}>;
  broadband: Array<{}>;
  schools: Array<{}>;
  courts: Array<{}>;
  userlat: number;
  userlng: number;
  postcode: string;

  constructor(
    private crimeService: CrimeService,
    private broadbandService: BroadbandService,
    private schoolService: SchoolService,
    private courtService: CourtService
  ) { }

  /*
    TODO - Strong suspect this is not the best way of doing this...one to
    investigate further at some point!
  */
  setupLocation(): void {
    const lat = localStorage.getItem('ngMkSnowmap.lat');
    const lng = localStorage.getItem('ngMkSnowmap.lng');
    const postcode = localStorage.getItem('ngMkSnowmap.postcode');
    this.userlat = parseFloat(lat);
    this.userlng = parseFloat(lng);
    this.postcode = postcode;
  }

  setPageToMain(): void {
    this.currentSet = 'main';
  }
  doCourts() {
    this.setupLocation();
    this.courtService.getCourts(this.postcode).subscribe((courts) => {
      this.courts = courts.contents.results;
      this.currentSet = 'courts';
      console.dir(this.courts);
    });
  }
  doSchools(): void {
    this.setupLocation();
    this.schoolService.getSchools(this.postcode).subscribe((schools) => {
      this.schools = schools.schools;
      this.currentSet = 'schools';
    });
  }
 doBroadBand(): void {
   this.setupLocation();
   this.broadbandService.getBroadband(this.postcode).subscribe((broadband) => {
     this.broadband = broadband;
     this.currentSet = 'broadband';
   });
 }

  doCrime(): void {
    this.setupLocation();
    this.crimeService.getCrime(this.userlat, this.userlng).subscribe((crimes) => {
      this.crimes = crimes;
      this.currentSet = 'crime';
    });
  }
  ngOnInit() {
    if ( typeof(localStorage['ngMkSnowmap.lat']) !== 'undefined' ) {
      this.ready = true;
    }
  }
}
