import { Component, Input, OnInit, AfterViewInit } from '@angular/core';
import {DistanceAwayComponent} from '../../common/distance-away/distance-away.component';

@Component({
  selector: 'app-school-item-component',
  templateUrl: './school-item-component.component.html',
  styleUrls: ['./school-item-component.component.css']
})
export class SchoolItemComponentComponent implements AfterViewInit, OnInit {

  @Input() school: Object;
  @Input() userlat: number;
  @Input() userlng: number;
  units = 'miles';

  constructor() { }

  ngOnInit() {}

  ngAfterViewInit() {}
}
