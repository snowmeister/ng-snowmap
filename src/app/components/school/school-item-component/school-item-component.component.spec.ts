import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchoolItemComponentComponent } from './school-item-component.component';

describe('SchoolItemComponentComponent', () => {
  let component: SchoolItemComponentComponent;
  let fixture: ComponentFixture<SchoolItemComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchoolItemComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchoolItemComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
