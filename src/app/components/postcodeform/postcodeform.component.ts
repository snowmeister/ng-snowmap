import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PostcodeService } from '../../services/postcode/postcode.service';
import { Http } from '@angular/http';

@Component({
  selector: 'app-postcodeform',
  templateUrl: './postcodeform.component.html',
  styleUrls: ['./postcodeform.component.css']
})



export class PostcodeformComponent implements OnInit {

  @Output() reset = new EventEmitter();
  @Output() gotoMain = new EventEmitter();
  postcode: string;
  postcodeError: Boolean = false;
  postcodeErrorMessage = 'Oops! The postcode you have entered does not exist!';

  constructor( private postcodeService: PostcodeService) { }

  getLatLngFromPostcode () {

    const lsLat = localStorage.getItem('ngMkSnowmap.lat');
    const lsLng = localStorage.getItem('ngMkSnowmap.lng');
    const lspostcode = localStorage.getItem('ngMkSnowmap.postcode');

    event.preventDefault();
    this.postcodeService.getLatLngFromPostcode(this.postcode).subscribe(
      postcode => {
        this.postcodeError = false;
        localStorage.setItem('ngMkSnowmap.lat', postcode.result.latitude);
        localStorage.setItem('ngMkSnowmap.lng', postcode.result.longitude);
        localStorage.setItem('ngMkSnowmap.postcode', postcode.result.postcode);
        this.gotoMain.emit('redirectToMain');
      },
      err => {
        this.postcodeError = true;
      }
    );
  }

  togglePostcodeForm() {
    this.reset.emit('togglePostcodeForm');
  }
  ngOnInit() { }
}
