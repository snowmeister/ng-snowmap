import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostcodeformComponent } from './postcodeform.component';

describe('PostcodeformComponent', () => {
  let component: PostcodeformComponent;
  let fixture: ComponentFixture<PostcodeformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostcodeformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostcodeformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
