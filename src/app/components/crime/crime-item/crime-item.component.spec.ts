import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrimeItemComponent } from './crime-item.component';

describe('CrimeItemComponent', () => {
  let component: CrimeItemComponent;
  let fixture: ComponentFixture<CrimeItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrimeItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrimeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
