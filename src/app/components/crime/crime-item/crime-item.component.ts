import { Component, OnInit, Input } from '@angular/core';
import {CrimeTitleComponent} from '../crime-title/crime-title.component';
import {DistanceAwayComponent} from '../../common/distance-away/distance-away.component';
@Component({
  selector: 'app-crime-item',
  templateUrl: './crime-item.component.html',
  styleUrls: ['./crime-item.component.css']
})
export class CrimeItemComponent implements OnInit {

  @Input() crime: object;
  @Input() userlat: number;
  @Input() userlng: number;
  units = 'miles';
  constructor() { }

  ngOnInit() {
  }

}
