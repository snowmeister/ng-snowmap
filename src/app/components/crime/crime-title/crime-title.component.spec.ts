import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrimeTitleComponent } from './crime-title.component';

describe('CrimeTitleComponent', () => {
  let component: CrimeTitleComponent;
  let fixture: ComponentFixture<CrimeTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrimeTitleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrimeTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
