import { Component, Input, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-crime-title',
  templateUrl: './crime-title.component.html',
  styleUrls: ['./crime-title.component.css']
})
export class CrimeTitleComponent implements AfterViewInit {

  @Input() title: string;
  strTitle = '';

  toProperCase(arr: Array<string>) {
    return arr.map(function(word){
      return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
    }).join(' ');
  }

  constructor() { }


  ngAfterViewInit(): void {
    setTimeout(function(){
      this.strTitle = (this.toProperCase(this.title.split('-')));
    }.bind(this), 1);
  }

}
