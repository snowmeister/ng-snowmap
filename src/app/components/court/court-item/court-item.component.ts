import { Component, OnInit, Input, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-court-item',
  templateUrl: './court-item.component.html',
  styleUrls: ['./court-item.component.css']
})
export class CourtItemComponent implements OnInit, AfterViewInit {

  @Input() court: object;
  @Input() userlat: number;
  @Input() userlng: number;
  units = 'miles';

  constructor() { }

  ngOnInit() {}

  ngAfterViewInit() {
    console.dir(this.court);
  }
}
