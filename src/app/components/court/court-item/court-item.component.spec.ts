import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourtItemComponent } from './court-item.component';

describe('CourtItemComponent', () => {
  let component: CourtItemComponent;
  let fixture: ComponentFixture<CourtItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourtItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourtItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
