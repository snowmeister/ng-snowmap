import { Component, OnInit } from '@angular/core';
import { PostcodeformComponent } from '../postcodeform/postcodeform.component';
import { RouterModule } from '@angular/router';
import { LocationService } from '../../services/location/location.service';
import { PostcodeService } from '../../services/postcode/postcode.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  usePostCode: Boolean = false;
  userlocation: UserLocation;
  gettingLocation: Boolean = false;
  choosePostcode: Boolean = false;
  postcodes: Array<{}>;
  tempPosition: Array<number>;

  constructor(private locationService: LocationService, private postcodeService: PostcodeService ) { }

  updateUserLocation(lat: number, lng: number, postcode: string) {
    localStorage.setItem('ngMkSnowmap.lat', lat.toString());
    localStorage.setItem('ngMkSnowmap.lng', lng.toString());
    localStorage.setItem('ngMkSnowmap.postcode', postcode);
    this.userlocation = {
      lat: lat,
      lng: lng,
      postcode: postcode
    };
    this.redirectToMain();
  }

  getPostcodeFromLatLng(lat: number, lng: number): void {
    this.postcodeService.getPostcodeFromLatLng(lat, lng).subscribe((postcodes) => {
      this.choosePostcode = true;
      this.postcodes = postcodes.result;
    });
  }

  getUserLocation(): void {
    this.gettingLocation = true;
    this.locationService.getLocation().forEach((position: Position) => {
      this.tempPosition = [position.coords.latitude, position.coords.longitude];
      this.getPostcodeFromLatLng(position.coords.latitude, position.coords.longitude);
    }).then(function() {
      this.gettingLocation = false;
    }.bind(this));
  }

  togglePostcodeForm() {
    this.usePostCode = !this.usePostCode;
  }

  selectPostcode(postcode: string) {
    this.updateUserLocation(this.tempPosition[0], this.tempPosition[1], postcode);
  }

  redirectToMain() {
    /*
      TODO - THIS IS BAD - Need to figure out how to do this the ANGULAR WAY!
      Keeping it like this restricts the app to environments that have a WINDOW object!
      */
      window.location.href = '/main';
  }
  ngOnInit() {
    this.usePostCode = false;
  }
}

interface UserLocation {
  lat: number;
  lng: number;
  postcode: string;
}
