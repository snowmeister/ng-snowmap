import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DistanceAwayComponent } from './distance-away.component';

describe('DistanceAwayComponent', () => {
  let component: DistanceAwayComponent;
  let fixture: ComponentFixture<DistanceAwayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DistanceAwayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistanceAwayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
