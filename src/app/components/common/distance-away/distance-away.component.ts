import { Component, Input, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-distance-away',
  templateUrl: './distance-away.component.html',
  styleUrls: ['./distance-away.component.css']
})
export class DistanceAwayComponent implements AfterViewInit {

  @Input() lat1: number;
  @Input() lat2: number;
  @Input() lng1: number;
  @Input() lng2: number;
  @Input() units: string;

  location: Array<number>;
  distance: String;
  kilometersToMiles(kilometers) {
    let miles;
    miles = (kilometers * 0.62137);
    miles = Math.round(miles * 100) / 100;
    return miles;
  }

  milesToKilometers(miles) {
    let km;
    km = (miles / 0.62137);
    km = Math.round(km * 100) / 100;
    return km;
  }
  deg2rad(deg) {
    return deg * (Math.PI / 180);
  }
  getDistanceFromLatLonInMiles(lat1, lon1, lat2, lon2) {
    const R = 6371; // Radius of the earth in km
    const dLat = this.deg2rad(lat2 - lat1); // deg2rad below
    const dLon = this.deg2rad(lon2 - lon1);
    const a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
    + Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const d = this.kilometersToMiles(R * c); // Distance in km
    return d.toFixed(2);
  }

  constructor() { }

  ngAfterViewInit() {

    setTimeout(function(){
      this.distance = this.getDistanceFromLatLonInMiles(this.lat1, this.lng1, this.lat2, this.lng2) + ' ' + this.units;
    }.bind(this), 1);
  }

}
